package net.devstudio.setthings.services;

import net.devstudio.setthings.SettingsMaker;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

public class BatteryReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
        }

        Intent i = context.getApplicationContext().registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        int level = i.getIntExtra(BatteryManager.EXTRA_LEVEL, 1);
        int scale = i.getIntExtra(BatteryManager.EXTRA_SCALE, 1);
        int perc = (int) (100 * (level / (double) scale));

        /*Log.e("AlarmReceiver", "Battery Percent: " + perc + "; LEVEL=" + level
				+ "; SCALE=" + scale);*/

        if(SettingsMaker.batteryValue != perc){
            SettingsMaker.batteryValue = perc;

            Intent service = new Intent(context.getApplicationContext(), SettingsMaker.class);
            context.startService(service);
        }
    }

}
