package net.devstudio.setthings.services;

import net.devstudio.setthings.SettingsMaker;
import net.devstudio.setthings.conditions.ConditionsOrientationActivity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.IBinder;
import android.util.Log;

public class OrientationService extends Service {

	private static final String TAG = "Orientation Service";
	private static boolean isRunning=false;// if service is running

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "onCreate()");

		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_CONFIGURATION_CHANGED);
		this.registerReceiver(mBroadcastReceiver, filter);
		OrientationService.isRunning = true;
	}

	public BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Intent.ACTION_CONFIGURATION_CHANGED)) {
				Log.d("OrientationChanged Broadcast", "received->"
						+ Intent.ACTION_CONFIGURATION_CHANGED);

				if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
					Log.d("OrientationChanged Broadcast", "LANDSCAPE");
					SettingsMaker.orientationValue = ConditionsOrientationActivity.horizontal;
				} else {
					Log.d("OrientationChanged Broadcast", "PORTRAIT");
					SettingsMaker.orientationValue = ConditionsOrientationActivity.vertical;
				}

				//SettingsMaker.getInstance(getApplicationContext()).update();
				Intent service = new Intent(context.getApplicationContext(), SettingsMaker.class);
				startService(service);
			} else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
				context.startService(new Intent(context,
						OrientationService.class));
			}
		}
	};

	@Override
	public void onDestroy() {
		unregisterReceiver(mBroadcastReceiver);
		OrientationService.isRunning = false;
		Log.d(TAG, "onDestroy()");
	}

	@Override
	public void onStart(Intent intent, int startid) {
		Log.d(TAG, "onStart()");
	}

	public static boolean isRunning(){
		return isRunning;
	}
	
	// source:
	// http://stackoverflow.com/questions/4625167/how-do-i-use-a-service-to-monitor-orientation-change-in-android
	// change with this: 
	// http://www.41post.com/4696/programming/android-obtaining-the-current-orientation-using-a-broadcastreceiver
}
