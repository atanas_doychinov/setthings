package net.devstudio.setthings.conditions;

import net.devstudio.setthings.R;
import net.devstudio.setthings.db.ConditionManager;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

public class ConditionsOrientationActivity extends Activity {

    String orientation;
    boolean isUpdate;
    long sitId;
    String title;
    ConditionManager conditionsManager;

    public static final String horizontal = "Landscape";
    public static final String vertical = "Portrait";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conditions_orientation);

        orientation = horizontal;
        final RadioButton hRadio = (RadioButton) findViewById(R.id.horizontal_orientation);
        final RadioButton vRadio = (RadioButton) findViewById(R.id.vertical_orientation);

        OnClickListener radioHorizontalListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                orientation=horizontal;
            }
        };

        OnClickListener radioVerticalListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                orientation=vertical;
            }
        };

        hRadio.setOnClickListener(radioHorizontalListener);
        vRadio.setOnClickListener(radioVerticalListener);

        conditionsManager = new ConditionManager(ConditionsOrientationActivity.this);
        sitId = getIntent().getLongExtra("situationId", -1);
        title = getResources().getResourceEntryName(R.string.orientation);
        isUpdate = conditionsManager.hasThisCondition(title, sitId);
        if(isUpdate){
            String note = conditionsManager.getNote(title, sitId);
            if(note.equals(horizontal)){
                hRadio.setChecked(true);
                orientation=horizontal;
            }
            else if(note.equals(vertical)){
                vRadio.setChecked(true);
                orientation=vertical;
            }
        }
    }

    @Override
    public void onBackPressed() {
        // now this is in SettingsMaker
        //startService( new Intent(this, OrientationService.class) );

        String desc = "";
        if(orientation.equals(horizontal)){
            desc=getString(R.string.horizontal);
        }else{
            desc=getString(R.string.vertical);
        }

        if(isUpdate){
            conditionsManager.updateCondition(title, sitId, desc, orientation);
        }
        else{
            conditionsManager.addCondition(title, sitId, desc, orientation);
        }

        conditionsManager.stop();
        super.onBackPressed();
    }


}
