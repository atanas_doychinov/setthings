/**
 * 
 */
package net.devstudio.setthings.conditions;

import java.util.ArrayList;
import java.util.HashMap;

import net.devstudio.setthings.R;
import net.devstudio.setthings.db.ConditionManager;
import net.devstudio.setthings.services.BatteryReceiver;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class ConditionsBatteryActivity extends Activity {
    ConditionManager conditionsManager;
    TextView percentText;

    private boolean isUpdate;
    private String title;
    private long sitId;

    private String sign;
    private String perc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conditions_battery);

        conditionsManager = new ConditionManager(ConditionsBatteryActivity.this);

        Intent intent = getIntent();
        sitId = intent.getLongExtra("situationId", -1);
        title = getResources().getResourceEntryName(R.string.battery);
        isUpdate = conditionsManager.hasThisCondition(title, sitId);

        Spinner spinner = (Spinner) findViewById(R.id.spinner_battery);
        spinner.setAdapter(this.getAdapter());
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView,
                    View selectedItemView, int position, long id) {
                TextView tvSign = (TextView) findViewById(R.id.dialog_text);
                tvSign.setTextColor(Color.BLUE);
                switch(position){
                    case 0:
                        sign = "<";
                        break;
                    case 1:
                        sign = ">";
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        percentText = (TextView) findViewById(R.id.battery_percent);

        SeekBar seekBar = (SeekBar) findViewById(R.id.progressbar_battery);
        seekBar.setProgress(50);

        if (!isUpdate) {
            sign = "<";
            perc = "50";
            percentText.setText(perc + " %");
            seekBar.setProgress(50);
            spinner.setSelection(0);
        } else {
            String desc = conditionsManager.getNote(title, sitId);
            parserBattery(desc);
            percentText.setText(perc + " %");
            seekBar.setProgress(Integer.valueOf(perc));
            if (sign.equals("<")) {
                spinner.setSelection(0);
            } else {
                spinner.setSelection(1);
            }
        }

        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
                String valueProgress = String.valueOf(progress);
                percentText.setText(valueProgress + " %");
                perc = valueProgress;
            }
        });

    }

    private SpinnerAdapter getAdapter() {

        String titleArray[] = { getString(R.string.below),
                getString(R.string.above) };
        int valueArray[] = { R.drawable.less, R.drawable.more };
        ArrayList<HashMap<String, String>> myList = new ArrayList<HashMap<String, String>>();

        HashMap<String, String> map;
        for (int i = 0; i < titleArray.length; i++) {
            map = new HashMap<String, String>();
            map.put("iconId", valueArray[i] + "");
            map.put("title", titleArray[i]);
            myList.add(map);
        }

        SpinnerAdapter adapter = new SimpleAdapter(this, myList,
                R.layout.row_dialog, new String[] { "iconId", "title" },
                new int[] { R.id.dialog_icon, R.id.dialog_text });

        return adapter;
    }

    @Override
    protected void onDestroy() {

        conditionsManager.stop();
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        String note = sign + " " + perc + " %";
        String desc = "";
        if (sign.equals("<")) {
            desc = getString(R.string.below);
        } else if (sign.equals(">")) {
            desc = getString(R.string.above);
        }
        desc = desc.concat(" ").concat(perc).concat(" %");
        if (!isUpdate) {
            conditionsManager.addCondition(title, sitId, desc, note);
        } else {
            conditionsManager.updateCondition(title, sitId, desc, note);
        }

        Intent alarm = new Intent(ConditionsBatteryActivity.this,
                BatteryReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, alarm, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis(), 10 * 60 * 1000, pi);// 10 minutes

        super.onBackPressed();
    }

    public void parserBattery(String data) {
        if (data.contains("<")) {
            sign = "<";
        } else {
            sign = ">";
        }

        int start = data.indexOf(" ");
        int end = data.indexOf(" %");

        perc = data.substring(start + 1, end);

    }
}
