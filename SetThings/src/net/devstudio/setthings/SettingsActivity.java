
package net.devstudio.setthings;

import net.devstudio.setthings.utils.C;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

public class SettingsActivity extends Activity {

    private EditText etDefaultRadius;

    private EditText etCheckTime;

    private CheckBox cbShowVolume;

    private RadioButton rbMeterUnit;
    private RadioButton rbFeetUnit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        rbMeterUnit = (RadioButton)findViewById(R.id.settings_radio_meter);
        rbFeetUnit = (RadioButton) findViewById(R.id.settings_radio_feets);
        etDefaultRadius = (EditText)findViewById(R.id.default_radius);
        etCheckTime = (EditText)findViewById(R.id.check_freq_time);
        cbShowVolume = (CheckBox)findViewById(R.id.show_volume_changes);
        Button btnRestore = (Button)findViewById(R.id.set_default_button);
        Button btnSave = (Button)findViewById(R.id.save_button);

        btnRestore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                C.RADIUS_IN_METERS = 100;
                C.CHECK_TIME_IN_MINUTES = 3;
                cbShowVolume.setChecked(false);
                etDefaultRadius.setText(String.valueOf(C.RADIUS_IN_METERS));
                etCheckTime.setText(String.valueOf(C.CHECK_TIME_IN_MINUTES));
                rbMeterUnit.setChecked(true);
            }
        });

        btnSave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String defaultRadius = etDefaultRadius.getText().toString();
                boolean isMeterChecked = rbMeterUnit.isChecked();
                String defaultTime = etCheckTime.getText().toString();
                setDefaultRadiusValue(defaultRadius);
                setDefaultCheckingTime(defaultTime);
                SharedPreferences sharedPreferences = getSharedPreferences(C.prefs.PREFS_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt(C.prefs.radius_unit, isMeterChecked ? 0 : 1);
                editor.putInt(C.prefs.default_radius, Integer.valueOf(defaultRadius));
                editor.putInt(C.prefs.checking_time, Integer.valueOf(defaultTime));
                editor.putBoolean(C.prefs.show_volume, cbShowVolume.isChecked());
                editor.commit();

                finish();
            }
        });

        loadPreferences();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new Builder(SettingsActivity.this);
        builder.setMessage(R.string.no_save);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                SettingsActivity.this.finish();
            }
        });
        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();

    }

    protected void setDefaultRadiusValue(String r) {
        if (r.equals("")) {
            C.RADIUS_IN_METERS = 100;
        } else {
            C.RADIUS_IN_METERS = Integer.valueOf(r);
        }
    }

    protected void setDefaultCheckingTime(String time) {
        if (time.equals("")) {
            C.CHECK_TIME_IN_MINUTES = 3;
        } else {
            C.CHECK_TIME_IN_MINUTES = Integer.valueOf(time);
        }
    }


    private void loadPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences(C.prefs.PREFS_NAME, MODE_PRIVATE);
        C.RADIUS_IN_METERS = sharedPreferences.getInt(C.prefs.default_radius, 100);
        C.CHECK_TIME_IN_MINUTES = sharedPreferences.getInt(C.prefs.checking_time, 3);
        boolean isShowing = sharedPreferences.getBoolean(C.prefs.show_volume, false);
        int unit = sharedPreferences.getInt(C.prefs.radius_unit, 0);
        if(unit == 0){
            rbMeterUnit.setChecked(true);
        } else{
            rbFeetUnit.setChecked(true);
        }
        cbShowVolume.setChecked(isShowing);
        etDefaultRadius.setText(String.valueOf(C.RADIUS_IN_METERS));
        etCheckTime.setText(String.valueOf(C.CHECK_TIME_IN_MINUTES));
    }
}
