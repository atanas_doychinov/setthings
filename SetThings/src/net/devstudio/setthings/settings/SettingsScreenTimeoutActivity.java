package net.devstudio.setthings.settings;

import static android.provider.Settings.System.SCREEN_OFF_TIMEOUT;
import net.devstudio.setthings.R;
import net.devstudio.setthings.db.SettingManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.SettingNotFoundException;
import android.view.KeyEvent;
import android.widget.ArrayAdapter;

public class SettingsScreenTimeoutActivity extends Activity{
    AlertDialog alert;
    private SettingManager settingsManager;
    private boolean isUpdate;
    private String title;
    private long sitId;
    private int checked;

    final long msTimes[]={15000,30000, 60000, 120000, 600000, 1800000};
    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = ArrayAdapter.createFromResource(
                this,R.array.timeout_period, android.R.layout.select_dialog_singlechoice);

        settingsManager = new SettingManager(SettingsScreenTimeoutActivity.this);
        Intent intent = getIntent();
        sitId = intent.getLongExtra("situationId", -1);
        title = getResources().getResourceEntryName(R.string.screen_timeout);
        isUpdate = settingsManager.hasThisSetting(title, sitId);

        int timeoutTime = 0;
        checked = -1;

        if(isUpdate){
            String note = settingsManager.getNote(title, sitId);
            timeoutTime = Integer.valueOf(note);
            for(int i = 0; i<msTimes.length; i++){
                if(msTimes[i] == timeoutTime){
                    checked = i;
                }
            }
        }
        // Get the current screen timeout...
        else
            try {
                timeoutTime=android.provider.Settings.System.getInt(getContentResolver(), SCREEN_OFF_TIMEOUT);
            } catch (SettingNotFoundException e) {
                e.printStackTrace();
            }

        // Set screen timeout to 10 seconds...
        //android.provider.Settings.System.putInt(getContentResolver(), SCREEN_OFF_TIMEOUT, timeoutTime);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.screen_timeout);
        builder.setSingleChoiceItems(adapter, checked, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                checked = item;
                dialog.dismiss();
                onBackPressed();
            }
        });
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK){
                    dialog.cancel();
                    finish();
                }
                return false;
            }
        });

        alert = builder.create();
        alert.show();

    }

    @Override
    public void onBackPressed() {
        String desc="";
        String note="";

        if(checked!=-1){
            desc = adapter.getItem(checked).toString();
            note = String.valueOf(msTimes[checked]);
        }
        else {
            finish();
        }

        if (!isUpdate) {
            settingsManager.addSetting(title, sitId, desc, note);
        } else {
            settingsManager.updateSetting(title, sitId, desc, note);
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        settingsManager.stop();
        super.onDestroy();
    }


}
