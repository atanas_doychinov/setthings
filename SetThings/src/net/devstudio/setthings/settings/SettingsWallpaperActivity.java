package net.devstudio.setthings.settings;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import net.devstudio.setthings.R;
import net.devstudio.setthings.db.SettingManager;
import net.devstudio.setthings.utils.Base64;
import net.devstudio.setthings.utils.C;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

public class SettingsWallpaperActivity extends Activity {
    private WallpaperManager wallpaperManager;
    private Drawable wallpaperDrawable;
    private SettingManager settingsManager;
    private boolean isUpdate;
    private String title;
    private long sitId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        wallpaperManager = WallpaperManager.getInstance(this);
        // Get current wallpaper to return it after the user's choice
        wallpaperDrawable = wallpaperManager.getDrawable();

        settingsManager = new SettingManager(SettingsWallpaperActivity.this);
        Intent intent = getIntent();
        sitId = intent.getLongExtra("situationId", -1);
        title = getResources().getResourceEntryName(R.string.wallpaper);
        isUpdate = settingsManager.hasThisSetting(title, sitId);

        Intent i = new Intent(Intent.ACTION_SET_WALLPAPER);
        startActivityForResult(i, C.WALLPAPER_REQUEST);

        /*try{
		Intent i = new Intent(Intent.ACTION_GET_CONTENT);
		i.setType("image/*");
		startActivityForResult(Intent.createChooser(i, "Select Picture"),  2);
	} catch (Exception e) {
        Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
        Log.e(e.getClass().getName(), e.getMessage(), e);
    }*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == C.WALLPAPER_REQUEST && resultCode == Activity.RESULT_OK){
            // Get the new wallpaper which the user is set by choosing it
            Drawable newWallpaper = wallpaperManager.getDrawable();
            // Current wallpaper is placed in:  /data/data/com.android.settings/files/wallpaper

            // Set the old wallpaper
            if(wallpaperDrawable!=null){
                Bitmap bitmap = ((BitmapDrawable)wallpaperDrawable).getBitmap();

                try {
                    wallpaperManager.setBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // Save the selected wallpaper
            String note = SettingsWallpaperActivity.drawableToString(newWallpaper);

            if(isUpdate){
                settingsManager.updateSetting(title, sitId, "", note);
            }else{
                settingsManager.addSetting(title, sitId, "", note);
            }
        }

        finish();

        super.onActivityResult(requestCode, resultCode, data);
    }

    public static String drawableToString(Drawable newWallpaper){
        Bitmap newBitmap = ((BitmapDrawable) newWallpaper).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] imageInByte = stream.toByteArray();
        String note=Base64.encodeBytes(imageInByte);
        return note;
    }

    @Override
    protected void onDestroy() {
        settingsManager.stop();
        super.onDestroy();
    }


}
// http://stackoverflow.com/questions/11081706/android-app-wallpaper-from-gallery
// Source: http://stackoverflow.com/questions/10566975/getting-all-system-wallpapers