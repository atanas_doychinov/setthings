package net.devstudio.setthings.settings;

import net.devstudio.setthings.R;
import net.devstudio.setthings.db.SettingManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ToggleButton;

public class SettingsBluetoothActivity extends Activity {
    private SettingManager settingsManager;
    private boolean isUpdate;
    private String title;
    private long sitId;

    boolean isBluetoothOn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_bluetooth);

        ToggleButton bluetoothButton = (ToggleButton) findViewById(R.id.bluetooth_toggle);

        settingsManager = new SettingManager(SettingsBluetoothActivity.this);
        Intent intent = getIntent();
        sitId = intent.getLongExtra("situationId", -1);
        title = getResources().getResourceEntryName(R.string.bluetooth);
        isUpdate = settingsManager.hasThisSetting(title, sitId);

        isBluetoothOn = false;

        if(isUpdate){
            String note = settingsManager.getNote(title, sitId);
            if(note.equals("On")){
                isBluetoothOn=true;
            }
            else{
                isBluetoothOn=false;
            }
        }
        bluetoothButton.setChecked(isBluetoothOn);

    }

    public void onBluetoothClick(View v) {
        isBluetoothOn = ((ToggleButton) v).isChecked();
    }

    @Override
    public void onBackPressed() {
        String note="";
        String desc="";
        if (isBluetoothOn){
            note="On";
            desc=getString(R.string.toggle_on);
        }else{
            note="Off";
            desc=getString(R.string.toggle_off);
        }

        if (!isUpdate) {
            settingsManager.addSetting(title, sitId, desc, note);
        } else {
            settingsManager.updateSetting(title, sitId, desc, note);
        }

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        settingsManager.stop();
        super.onDestroy();
    }


}
