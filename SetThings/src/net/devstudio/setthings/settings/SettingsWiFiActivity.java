package net.devstudio.setthings.settings;

import net.devstudio.setthings.R;
import net.devstudio.setthings.db.SettingManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ToggleButton;

public class SettingsWiFiActivity extends Activity {
    private SettingManager settingsManager;
    private boolean isUpdate;
    private String title;
    private long sitId;

    private boolean isWiFiOn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_wi_fi);

        ToggleButton bluetoothButton = (ToggleButton) findViewById(R.id.wi_fi_toggle);

        settingsManager = new SettingManager(SettingsWiFiActivity.this);
        Intent intent = getIntent();
        sitId = intent.getLongExtra("situationId", -1);
        title = getResources().getResourceEntryName(R.string.wi_fi);
        isUpdate = settingsManager.hasThisSetting(title, sitId);

        isWiFiOn = false;

        if(isUpdate){
            String note = settingsManager.getNote(title, sitId);
            isWiFiOn = (note.equals("On")) ? true : false;
        }
        bluetoothButton.setChecked(isWiFiOn);
    }

    public void onWiFiClick(View v){
        isWiFiOn = ((ToggleButton) v).isChecked();

    }

    @Override
    public void onBackPressed() {
        String note="";
        String desc="";
        if (isWiFiOn){
            note="On";
            desc=getString(R.string.toggle_on);
        }else{
            note="Off";
            desc=getString(R.string.toggle_off);
        }

        if (!isUpdate) {
            settingsManager.addSetting(title, sitId, desc, note);
        } else {
            settingsManager.updateSetting(title, sitId, desc, note);
        }

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        settingsManager.stop();
        super.onDestroy();
    }
}
