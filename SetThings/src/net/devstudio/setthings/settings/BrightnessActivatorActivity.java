package net.devstudio.setthings.settings;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.WindowManager;

public class BrightnessActivatorActivity extends Activity {

	private static final int DELAYED_MESSAGE = 1;

	private Handler handler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == DELAYED_MESSAGE) {
					BrightnessActivatorActivity.this.finish();
				}
				super.handleMessage(msg);
			}
		};
		
		/*// set brightness to MANUAL
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if(currentapiVersion >= 10){
			int brightnessMode = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE);
			if (brightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
			    Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
			}
		}*/
		
		// get extras
		String brightness = getIntent().getStringExtra("brightnessValue");
		
		// change user setting
		Settings.System.putInt(getContentResolver(),
				Settings.System.SCREEN_BRIGHTNESS,
				Integer.valueOf(brightness));

		// set screen
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		// from 0 to 1
		lp.screenBrightness = Float.valueOf(brightness) / 255.0f;
		getWindow().setAttributes(lp);

		Message message = handler.obtainMessage(DELAYED_MESSAGE);
		// this next line is very important, you need to finish your activity
		// with slight delay
		handler.sendMessageDelayed(message, 500);
	}

}
