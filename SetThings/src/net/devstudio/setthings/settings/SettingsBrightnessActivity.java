package net.devstudio.setthings.settings;

import net.devstudio.setthings.R;
import net.devstudio.setthings.db.SettingManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class SettingsBrightnessActivity extends Activity {
    TextView percent;
    private int mBrightness;
    private String desc;

    private SettingManager settingsManager;
    private boolean isUpdate;
    private String title;
    private long sitId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_brightness);

        settingsManager = new SettingManager(SettingsBrightnessActivity.this);
        Intent intent = getIntent();
        sitId = intent.getLongExtra("situationId", -1);
        title = getResources().getResourceEntryName(R.string.brightness);
        isUpdate = settingsManager.hasThisSetting(title, sitId);

        final SeekBar mProgress = (SeekBar) findViewById(R.id.progressbar_brightness);
        percent = (TextView) findViewById(R.id.progress_percent);

        if (isUpdate) {
            String note = settingsManager.getNote(title, sitId);
            mBrightness = Integer.valueOf(note);
        } else {
            try {
                // return between 0 and 255
                mBrightness = Settings.System.getInt(getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS);
            } catch (SettingNotFoundException e) {
                e.printStackTrace();
            }
        }

        // set current brightness
        WindowManager.LayoutParams settings = getWindow()
                .getAttributes();
        // from 0 to 1
        settings.screenBrightness = mBrightness / 255.0f;
        getWindow().setAttributes(settings);

        int curProgress = (int) Math.ceil((mBrightness / 2.55));
        mProgress.setProgress(curProgress);
        desc = curProgress + " %";
        percent.setText(desc);

        mProgress.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int p = seekBar.getProgress();
                if(p == 0) {
                    seekBar.setProgress(1);
                    p=1;
                }
                mBrightness = (int) (p * 2.55);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
                if(progress == 0){progress = 1;}
                desc = String.valueOf(progress) + " %";
                percent.setText(desc);

                WindowManager.LayoutParams settings = getWindow()
                        .getAttributes();
                // from 0 to 1
                settings.screenBrightness = progress / 100.0f;
                getWindow().setAttributes(settings);
            }
        });

    }

    @Override
    public void onBackPressed() {
        String note = String.valueOf(mBrightness);
        if (!isUpdate) {
            settingsManager.addSetting(title, sitId, desc, note);
        } else {
            settingsManager.updateSetting(title, sitId, desc, note);
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        settingsManager.stop();
        super.onDestroy();
    }

}
