This app changes your phone settings under given circumstances. For example you can choose to automatically set the sounds to Off when you are at school or at work. This is just an example of the power of this application. It is like "Locale", but absolutly free.

What you can do with this app?

1. Add Conditions:
	- Battery power
	- Become a call from number
	- Locations
	- Phone orientation
	- Time

	
2. Add Settings:
	- Switch On/Off Bluetooth if the phone support it
	- Controll the screen brightness
	- Change the Ringtone
	- Change the screen timeout
	- Controll the sound volume
	- Change the wallpaper
	- Switch On/Off the Wi-Fi

[Link to the app in Google Play](https://play.google.com/store/apps/details?id=net.devstudio.setthings)